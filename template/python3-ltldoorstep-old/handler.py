from flask import current_app

import asyncio
import os
import errno

import logging
from dask import threaded
from ltldoorstep.config import set_config, load_config
from ltldoorstep.location_utils import load_berlin
import json
import tempfile
import requests
from flask_restful import Resource, abort, reqparse
from ltldoorstep.metadata import DoorstepContext
from ltldoorstep.config import load_config
from ltldoorstep.encoders import json_dumps
from ltldoorstep.errors import LintolDoorstepException
from ltldoorstep.reports.report import Report
import function.processor as mod


class Handler(Resource):
    _bucket = None
    _config = {}

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('filename', location='json')
        parser.add_argument('metadata', location='json')
        args = parser.parse_args()
        context = args['metadata']
        filename = args['filename']

        context = DoorstepContext.from_dict(json.loads(context))

        processor = mod.processor()

        with tempfile.NamedTemporaryFile(mode='w+b') as f:
            if 'metadataOnly' in context.configuration and context.configuration['metadataOnly']:
                filename = None
                current_app.logger.error("Metadata Only request.")
            else:
                current_app.logger.error("Full request.")
                if filename.startswith('https:') or filename.startswith('http:'):
                    r = requests.get(filename)
                    if r.status_code == 200:
                        for chunk in r.iter_content(chunk_size=1024):
                            f.write(chunk)
                else:
                    message = _(f'Filename is not URL: {filename}.')
                    current_app.logger.error(message)
                    e = FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)
                    e = LintolDoorstepException(
                        e,
                        processor=processor.code,
                        message=message
                    )
                    return {'error': 2, 'exception': json.loads(json_dumps(e))}
                f.flush()
                filename = f.name

            try:
                workflow = processor.build_workflow(filename, context)
                result = threaded.get(workflow, 'output', num_workers=2)
            except Exception as e:
                if not isinstance(e, LintolDoorstepException):
                    e = LintolDoorstepException(e)
                current_app.logger.error(e)
                return {'error': 1, 'exception': json.loads(json_dumps(e))}

            if isinstance(result, tuple) and len(result) == 2 and isinstance(result[1], Report):
                processor.set_report(result[1])
            elif isinstance(result, Report):
                processor.set_report(result)

            result = processor.compile_report(filename, context)

        return result

    @classmethod
    def preload(cls):
        cls._config = load_config()

        set_config('reference-data.storage', 'minio')
        set_config('storage.minio.region', 'us-east-1') # Fixed by Minio
        for k in ('bucket', 'key', 'secret', 'prefix', 'endpoint'):
            filename = os.path.join('/var', 'openfaas', 'secrets', f'minio_{k}')
            with open(filename, 'r') as f:
                value = f.read().strip()

            if k == 'prefix':
                set_config('reference-data.prefix', value)
            else:
                set_config(f'storage.minio.{k}', value)

        # e.g. load_berlin()
        if hasattr(mod, 'preload'):
            mod.preload()

        debug = cls._config['debug'] if 'debug' in cls._config else False
        logging.basicConfig(level=logging.DEBUG if debug else logging.INFO)
        cls.logger = logging.getLogger(__name__)

        return True
