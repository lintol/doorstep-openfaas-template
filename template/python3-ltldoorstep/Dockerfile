FROM debian:sid-slim

MAINTAINER Phil Weir <phil.weir@flaxandteal.co.uk>
# With acknowledgements to the maintainers of rehabstudio/docker-gunicorn-nginx:
#       Peter McConnell
#       Paddy Carey

RUN apt-get update && apt-get install -y \
      curl \
      build-essential git \
      python3-dev python3-setuptools \
      python3-pip libffi-dev \
      libyaml-dev \
      libssl-dev 'libstdc++-9-dev'

# Add non root user
RUN addgroup --system user && adduser --system --group user

WORKDIR /home/user/

USER user
ENV PATH=$PATH:/home/user/.local/bin

USER root

RUN apt-get update && apt-get install -y \
      cython gfortran libopenblas-dev>=0.3.0 \
      python3-pandas python3-grpcio \
      liblapack-dev pybind11-dev python3-scipy

USER user

# Allows you to add additional packages via build-arg
ARG ADDITIONAL_PACKAGE

USER root

# We split this out as pandas is a major build step, we don't want to have to reproduce for all ADDITIONAL_PACKAGEs
RUN apt update && apt install -y \
      ${ADDITIONAL_PACKAGE} cmake libxml2-dev libxslt-dev libgdal-dev proj-bin libproj-dev proj-data libgeos-dev gdal-bin gdal-data libfreetype-dev \
      libhdf5-dev libnetcdf-dev libjpeg-dev python3-pillow \
      python3-flask python3-sqlalchemy libspatialindex-dev \
      python3-gdal \
    && echo "Pulling watchdog binary from Github." \
    && curl -sSL https://github.com/openfaas-incubator/of-watchdog/releases/download/0.7.2/of-watchdog > /usr/bin/fwatchdog \
    && chmod +x /usr/bin/fwatchdog

#RUN git clone https://github.com/libspatialindex/libspatialindex /usr/src/libspatialindex \
#    && cd /usr/src/libspatialindex \
#    && cmake . -DCMAKE_INSTALL_PREFIX=/usr \
#    && make \
#    && make install \
#    && rm -rf /usr/src/libspatialindex

COPY index.py           .
COPY requirements.txt   .
COPY gunicorn_config.py .
COPY handler.py .

USER user

ENV PATH=$PATH:/home/user/.local/bin
RUN pip3 install --user ckanapi
RUN pip3 install --user -r requirements.txt

RUN mkdir -p function

USER root

WORKDIR /home/user/function/
COPY function/requirements.txt ./
USER user

RUN pip install --user -r requirements.txt

COPY function/requirements* ./
USER user

WORKDIR /home/user/

RUN if [ -f "requirements-latest.txt" ] ; then pip install --user -r requirements-latest.txt; fi

COPY function/*.py ./function/

ENV fprocess="gunicorn --config ./gunicorn_config.py index:app"
EXPOSE 5000

ENV cgi_headers="true"
ENV mode="http"
ENV upstream_url="http://127.0.0.1:5000"

HEALTHCHECK --interval=5s CMD [ -e /tmp/.lock ] || exit 1

ENTRYPOINT []

CMD ["fwatchdog"]
